import { useReducer } from 'react'

type TState<T> = {
  value: T;
}

type TReturn<T> = [T, (value: T) => void];

function reducer<T>(state: TState<T>, action: {type: string, value: T}) {
  return { value: action.value };
}

export const useState = <T>(value: T): TReturn<T>  => {
  const [state, dispatch] = useReducer(reducer, { value: value });

  return [state.value as T, (value: T) => dispatch({type: 'setValue', value: value})]
}