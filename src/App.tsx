import React, { useState } from 'react';
import { useState as useMyState } from './hooks/native/useState'

function App() {
  const [ state, setState ] = useState(0);
  const [ myState, setMyState ] = useMyState(0);
  const [ myState2, setMyState2 ] = useMyState(0);
  return <>
    <button onClick={() => setState(state + 1)}>{state}</button>
    <br/>
    <br/>
    <button onClick={() => setMyState(myState + 1)}>{myState}</button>
    <br/>
    <br/>
    <button onClick={() => setMyState2(myState2 + 1)}>{myState2}</button>
  </>;
}

export default App;
